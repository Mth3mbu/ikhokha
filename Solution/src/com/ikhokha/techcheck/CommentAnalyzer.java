package com.ikhokha.techcheck;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

public class CommentAnalyzer {
	
	private File file;
	
	public CommentAnalyzer(File file) {
		this.file = file;
	}
	
	public synchronized Map<String, Integer> analyze() {
		
		Map<String, Integer> resultsMap = new HashMap<>();
		
		try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
			
			String line = null;
			while ((line = reader.readLine()) != null) {
				analyzeShotComment(line, resultsMap);
				analyzeComment(line, resultsMap,"Mover","MOVER_MENTIONS");
				analyzeComment(line, resultsMap,"Shaker","SHAKER_MENTIONS");
				analyzeComment(line, resultsMap,"?","QUESTIONS");
				analyzeSpamComment(line, resultsMap);
			}
			
		} catch (FileNotFoundException e) {
			System.out.println("File not found: " + file.getAbsolutePath());
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println("IO Error processing file: " + file.getAbsolutePath());
			e.printStackTrace();
		}
		
		return resultsMap;
	}
	
	private void analyzeComment(String comment, Map<String, Integer> resultsMap,String searchTerm ,String mapKey)
	{
		var wordsArray = comment.toLowerCase().split(" ");
		searchTerm = searchTerm.toLowerCase();

		if(comment.toLowerCase().contains(searchTerm)){
			for (String word : wordsArray) {
				if(word.contains(searchTerm)){
					incOccurrence(resultsMap, mapKey);
				}
			}
		}
	}
	
	private void analyzeSpamComment(String comment,Map<String, Integer> resultsMap)
	{
		String regex = "\\(?\\b(http://|https://|www[.])[-A-Za-z0-9+&amp;@#/%?=~_()|!:,.;]*[-A-Za-z0-9+&amp;@#/%=~_()|]";
		Pattern pattern = Pattern.compile(regex);
		java.util.regex.Matcher matcher = pattern.matcher(comment);

		while(matcher.find()){
			incOccurrence(resultsMap, "SPAM");
		}
	}

	private void analyzeShotComment(String comment, Map<String, Integer> resultsMap){
		if (comment.length() < 15 && !comment.isEmpty()) {
			incOccurrence(resultsMap, "SHORTER_THAN_15");
		} 
	}

	/**
	 * This method increments a counter by 1 for a match type on the countMap. Uninitialized keys will be set to 1
	 * @param countMap the map that keeps track of counts
	 * @param key the key for the value to increment
	 */
	private void incOccurrence(Map<String, Integer> countMap, String key) {
		countMap.putIfAbsent(key, 0);
		countMap.put(key, countMap.get(key) + 1);
	}
}
