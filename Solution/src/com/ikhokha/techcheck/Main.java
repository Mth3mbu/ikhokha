package com.ikhokha.techcheck;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executors;

public class Main {

	public static void main(String[] args) {
		
		Map<String, Integer> totalResults = new HashMap<>();

		File docPath = new File("docs");
		File[] commentFiles = docPath.listFiles((d, n) -> n.endsWith(".txt"));

		var totalCpuCores = Runtime.getRuntime().availableProcessors();
		var executorService = Executors.newFixedThreadPool(totalCpuCores);

		for (File commentFile : commentFiles) {
			executorService.execute(new CommentAnalyzerTask(totalResults, commentFile));
		}

		executorService.shutdown();
		printResults(totalResults);
	}

	private static void printResults(Map<String, Integer> totalResults){
		try {

			Thread.sleep(100);
			System.out.println("RESULTS\n=======");
			totalResults.forEach((k,v) -> System.out.println(k + " : " + v));

		} catch (Exception e) {
			System.out.println("An error has occured due to:"+ e.getMessage());
		}
	}
}
