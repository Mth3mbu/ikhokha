package com.ikhokha.techcheck;

import java.io.File;
import java.util.Map;

public class CommentAnalyzerTask implements Runnable {
    private Map<String, Integer> totalResults;
    private File commentFile;

    public CommentAnalyzerTask(Map<String, Integer> totalResults, File commentFile) {
        this.totalResults = totalResults;
        this.commentFile = commentFile;
    }

    @Override
    public void run() {
        CommentAnalyzer commentAnalyzer = new CommentAnalyzer(commentFile);
        Map<String, Integer> fileResults = commentAnalyzer.analyze();

        addReportResults(fileResults, totalResults);
    }

    /**
     * This method adds the result counts from a source map to the target map
     * 
     * @param source the source map
     * @param target the target map
     */
    private void addReportResults(Map<String, Integer> source, Map<String, Integer> target) {

        for (Map.Entry<String, Integer> entry : source.entrySet()) {
            target.put(entry.getKey(),
                    target.containsKey(entry.getKey()) ? target.get(entry.getKey()) + entry.getValue()
                            : entry.getValue());
        }

    }
}